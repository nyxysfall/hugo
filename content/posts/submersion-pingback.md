+++
title = "Submersion (pingback from Sum)"
date = 2019-11-19T20:40:00-08:00
tags = ["aphotheosis", "collapse", "chaos-ecology"]
categories = ["theoryfic", "abysscom", "pingback"]
draft = false
type = "post"
+++

Here is a thing I wrote for Šum #12 on apocalyptic flooding and geopolitics http://sumrevija.si/en/issues/sum-12/
