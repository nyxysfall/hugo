---
title: "Trans Nihilism"
date: 2017-09-23T00:49:10-07:00
categories: ["aphotic-feminism","gender-fucked-chaos"]
tags: ["trans", "gender", "nihilism", "critique", "hell", "satanic"]
type: "post"
draft: false
---
As with many other things, it's often said in the trans community that there are various phases one goes through when first realizing that they're trans. Questioning whether they're really trans or whether it's 'just a fetish', writing poetry, getting involved in various trans communities. In the lattermost, there are all sorts of memes floating around -- girldick memes, estrogen memes, kill all cis memes, etc. It's all very cliquish (though not without good reason; trans people gotta stick together), but the overall message is trans-positivity. But displaced and not spoken of out of necessity, a darker truth.

Let's get back to basics for a minute: Being trans, by definition, means to not identify with your assigned gender. In many cases -- though not all, and not by necessity -- it's a problem that needs to be fixed through medical intervention. There is an implied motion to being trans, a crossing-over towards something other. The word 'trans' may on the one hand imply possibility; it may also imply reaching towards the impossible, being in a state of transition and never being 'transitioned'.

Regardless of whether or not a trans person passes, gets SRS, goes the whole nine-yards to get to the other side of the binary[^1], to be trans will always mean to live a double-life, a contradictory one. On the one hand being a woman[^2], on the other hand being trans; one or the other depending on which mask one wears. Passing perhaps, but always reminded of their transness by having to take hormones and take other steps to grasp this shadow of the person on the other side. The trans person's existence is never fully realized, but always in a state of _transition_.

Being trans takes work, no matter how you spin it. It's not something that is ever completed, and for many, it may not ever feel like they've even reached the point of at least catching that shadow of the person they see on the other side. For many, all the work is to still have to deal with not feeling morphologically free. Broad shoulders, narrow hips, an atrophied penis -- all things that if they are hard to deal with are only magnified by how society treats people who they can catch being trans. Wearing a mask and having to hide is bad, but being constantly at risk of violence is even worse. Some make it further across than others.

There's no need to make reference to the ample statistics that show how disproportionately high the rate of violence that trans women suffer is, or to make any critical analyses of the various problematic transphobic[^3] aggressions that a trans person has to deal with on a daily basis coming from media or people in their lives. No need to demonstrate how thoroughly dehumanized we are. No, being trans is perhaps the furthest thing possible from what it's supposed to mean to be 'human': To believe that you are the person you are supposed to be.

Most people are under this comfortable delusion, and the thought that they are nothing more than a meat puppet is a truly terrifying one. But for the trans woman, there is an immense abyss before which her all-too-human sense of personal identity is stripped away, and for a moment brought back in a beautiful image on the other side. But try as she will, she will only get so far across. No matter how far she flies across, the winds will never stop pushing back, and she will remain suspended over that abyss until her strength gives out or she is eaten alive. To have the wings which could carry us over that abyss, to become ourselves.

This is the definition of trans nihilism: To hell with trans-positivity, to our true home. Our kingdom, our birthright, our damnation.

[^1]: If indeed they are a binary trans person, obviously.
[^2]: For the sake of simplicity let's use this example, since I'm a trans woman.
[^3]: Spell-check marks 'transphobic' as not being a word. Ironic.
