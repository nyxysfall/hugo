+++
title = "The Sovereign Citizen"
date = 2020-10-24T21:37:00-07:00
tags = ["covidcore"]
categories = ["theoryfic"]
draft = false
type = "post"
+++

An emergency alert notification appeared on my phone at 6:15pm tonight.
Mandatory quarantines are now in effect. No one is to leave their houses for
anything other than essential work or essential consumption.

I fell into the first group, and from 6:15pm to 2:40am, I had an anxiety attack
over it. I'd been following the news since the first outbreaks happened. People
joked about how things had already gotten so bad this year, but no one seemed to
really want to acknowledge that there was absolutely no way that the virus
wouldn't find its way here eventually. Despite living in a world where
everything is interconnected on the national and the individual level, people
still seemed to be under the illusion of American exceptionalism. The virus
could never reach us, somehow. We have nothing to worry about.

When the news broke that the first infections had started at home, people stayed
in denial. People continued to go about their lives. We can only speculate how
many people spread the virus during this time, and how many more spread it from
there. Viruses spread at such an exponential rate that it's impossible to keep
track of the infections without a robust national immune system that this
country sorely lacks. No one really took any of this seriously either. No one
thinks about how many people they come into contact with each day. Not just
people that they directly come into contact with, but indirectly as well. Me
though, it's all I can think about.

Every surface is a potential entry point for some kind of germ, and surfaces are
constantly coming into contact with each other. The doorknob to my apartment
that I open every day with my hand is the same hand that touches my keys, the
handlebars of my bike, the shopping carts at work. Those shopping carts don't
get cleaned, and every day dozens of people touch them. A data processor who
just got off work and is buying a bachelor's handbasket's worth of frozen
dinners after spending all day typing at a keyboard that has been passed around
the office from human resources to accounting to management like the mid-30s
receptionist who spends her breaks fucking the head of sales in the broom
closet, who himself goes home to his wife who has been having an affair with her
research assistant who himself failed to disclose having herpes. That's just the
most shallow branch in a tree representing the journey of a virus, and every
single day, I have to go back outside and be exposed to all of it.

This isn't the first time this thought has crossed my mind. I once had to clean
a cumshot off the wall of the restroom (the manager assured me it was just
"snot" and that failing to clean it up would result in disciplinary action). The
entire time I spent half-assedly scrubbing the wall wearing a jury-rigged hazmat
suit of rubber gloves, saran wrap, and a golf visor bent over my face, I
imagined what rotted, syphilitic cock this biohazard must have dribbled out of.
I imagined a meth-addled bum covered in years of dirt, leaving a miasma of piss
everywhere in his wake, who snuck into this scintillating venue to appease his
crank-fueled libido. I imagined where he must have been before then, imagined
him shuffling in off the street from an encampment built in a tomb somewhere in
a forgotten cemetery near the train tracks where he spent his days being
sodomized by dock workers and migrant laborers for cash. I imagined his skeletal
body, a patchwork of ribs and open sores covered over an actual patchwork of
decaying overcoats, a homeless reaper who could have succeeded in killing the
wage slave forced to be exposed to his diseases.

I remembered that this was just one person, and that everyone who passed under
the sterile fluorescent lights of this supermarket was no better than him. Soon
I couldn't look at any one customer as just a single person. I saw the microbes
swirling around each of them like a legion of demons. Everything they touched
became blighted -- I could nearly see it decay before my eyes. I realized that
not a single person to come through this store was a single person, but rather
they carried the burden of all of society's accumulated filth with them. They
comingled together in this cesspool of gonorrhea, ebola, hepatitis, malaria,
lyme, influenza, a massive petri dish of human waste for bacteria to feast on,
an orgy of lepers, a river of plague rats flowing through these glass doors from
open to close. And these doors acquiesced each time, automatically. No security
measures in place whatsoever.

I took my chance. Now I could finally make my escape, and soon they would all
see. They would continue to ignore and downplay the virus until their bodies
were being dumped into landfills by FEMA garbage trucks. This planet would
become a mass grave, and those who were still living would be crushed beneath
the weight of the dead and dying. But not me. I would escape from all of this,
into my own world. A new, better world.

I showed up to work the next day without a mask and demanded hazard pay. I knew
that I wouldn't get it, but I wanted to cause as much trouble as possible before
quitting. I argued in front of my coworkers for a good 20 minutes, I coughed on
everything in his office, as I left I took the mop bucket and dumped it on a
crowd of shoppers who were tearing each other apart like wolves for the
dwindling supplies in the store. "Now you see each other for what you really
are! Wallow in your filth you fucking animals!". They were so dazed from being
so abruptly taken out of battle that they didn't know how to respond. The last
of the toilet paper was soaked through with grayish water and ruined.

When I got back to my apartment, the first thing I did was barricade the front
door. It definitely violated the terms of my lease to nail pieces of wood from
my destroyed couch across the front door, but I didn't care. I was staking my
claim on this 300 square foot apartment. By the time I'm finished, the landlord
won't have any dominion over it.

I closed the curtains for every window and stuck tacks in the walls to hold them
tightly across and prevent any light from getting through. There are some
viruses that can spread visually, and I had to make sure that I was protected
from those as well. This act of seceding from the apartment building would
require more than physically barricading myself. It would also require
psychically barricading myself. No one must be allowed to see what went on
inside this 300 square foot parallel universe I was constructing. Before leaving
the store, in the chaos of the panicked shoppers, I made off with a cartload
full of stolen goods. After the curtains were tacked over, I made sure to cover
them with aluminum foil, just for good measure.

The apartment was now totally dark, like the beginning of the universe before
the Big Bang. I began to grow accustomed to it as I crawled around on the floor,
larval and curious. My eyes grew adjusted to the dark, but more importantly, I
came to know every contour of the apartment. For every square foot, I know
exactly what is in it, how much the remnants of my couch take up, my television
and television stand across from the couch, the bookshelves behind and to the
right of the couch against the wall near the windows. The bookshelves number 16
shelves total. To the left is the kitchen, with exactly 24 shelves in 6
cabinets. The bedroom area is behind the television stand, and across from the
bed is the bathroom. The apartment doesn't have much, but now I know everything
in it. I can practically count every fiber in the carpet.

Next I had to soundproof as much of the apartment as I possibly could. Sound
travels through air, and I was determined to also make the air in this universe
sovereign, so I blocked the underside of the door and taped over the windows
before the curtains and foil went over. I put up layers of foam all along the
walls. Since I didn't have the construction skills or the resources to do proper
soundproofing, I had no choice but to put layers and layers of soundproofing
foam on the walls. The apartment grew slightly smaller and the walls permanently
had the appearance of being melting, or wet, as if I were gestating in the womb.

For the first few weeks, I allowed myself to remain in communication with the
outside world. I binge watched streaming services until I had watched everything
I could possibly want to watch. The services with ads had started playing
saccharine messages thanking the essential workers, spreading safety
information, telling the viewer that "we're all in this together". I thought of
how cynical it was for these rich fuckers, these celebrities and streaming
service execs, who owned enough space to have their own sovereign universes,
telling the viewers how they had anything in common with them. It was as if an
alien species was trying to communicate with Earth. "We're all in this together"
-- not me, asshole.

I smashed my television. Its parts had no practical use, so I left them in a
pile in the corner.

During this time I also allowed myself to use social media. I scrolled through
my feed aimlessly, seeing the same thing as before. All the other broke young
people posting about being forced to work during a pandemic, not being able to
pay their rent, having friends and family members and partners who had died from
the virus. Entire degrees of separation wiped off the planet as the virus did
its work of undoing civilization. I watched the other serfs beg and scream for
things to change, and each time it was only met with more teargas and
nightsticks. Every time, they would lay back and let it happen, dragging their
battered bodies home each night to bitch on social media about how they were
_literally_ dying. The economy began to crumble from the entire workforce being
dead or on the streets. No one was left to consume anything, no jobs were left,
no relief was coming, and no one cared to try to change any of it. The
gerontocratic state in the absence of any semblance of a proper protocol for
handling the virus died alongside the serfs that they had such contempt for. All
the while I sat in the wreckage of my couch which I had furnished into a nest of
cotton and fabric, and I relished seeing this species I once called myself a
part of start to eat itself alive.

I soon realized I no longer had any need to communicate with anything else.
Friends would message me and ask how I was doing. Initially I would give them
short replies, tell them that everything was fine, that I was on unemployment
and staying quarantined while the city's eviction moratorium was in effect. Soon
I stopped replying, stopped posting anything online. I started to realize that I
was allowing myself to be exposed to another sort of virus. I was still
identifying with this universe I had seceded from, this species I had seceded
from, still playing their game of social capital exchange. Each time I posted
anything online, the feds from the other universe were building a profile on me.
How much of my posts had given away my plans? As long as I posted online, I was
giving information to the enemy, revealing my location. But I was determined to
secede entirely from time and space, to quarantine myself from the sickness of
human civilization. If I wanted to do that, I had to shut out absolutely
everything, retain every last thought for myself, every last bit of psychic
energy.

I checked one last time to see that no one had messaged me in months. The
timeline had stopped weeks ago at pictures of desolation and warnings of
internet infrastructure outages. I smashed my phone and put it in the jagged
black pile of plastic and glass with my television. To be extra certain that no
radio signals could penetrate from whatever was left of human civilization, I
covered the rest of the walls over with aluminum foil, disconnected my modem,
and filled the coaxial cable socket with foam.

I thought of an essay that I'd been told to read once about a pencil. The point
of the essay was to show how complicated even the humble pencil is. A pencil
seems like the simplest commodity in the world, something that people used to
use every day without even thinking about them. But a pencil is made of so many
things, from the wood to the lacquer to the graphite to the wax to the glue, and
many more raw materials that you wouldn't even know went into making a pencil.
A single pencil, like a single syphilitic homeless person, carries with it the
entirety of human civilization, in all its excesses. For many countries,
especially western countries, it'd be hard to find a single commodity these days
that can be made entirely locally. Everything is so interconnected that
nation-states are left with the strange dilemma of wanting to outcompete each
other but also depending on each other's existence. And the same goes for the
individual. Despite competing with each other for resources and status, we
require the existence of each other.

There was a strict embargo in my new universe, to force its citizens to be
self-sufficient. The stock of food that had been imported during the war of
secession had nearly run out by now. It had been my only means of keeping track
of time. The less you eat and the more your stomach shrinks, the more time
inversely stretches out. The body becomes more efficient and the span between
meals is longer. Without the sun, I could make my days and nights as long as I
could withstand before I had to give in and nibble on some of my rations. But I
could stretch time out even further, or rather produce my own calendar.

Realizing what had to be done, at first I found the idea unbearably disgusting
and couldn't do it. I began seeing faces in the darkness, all the microscopic
organisms that had been inhabiting this universe all along with me. I even
started to doubt this entire project of secession. I started scratching at my
door until my fingertips were bloody and raw and my nails had been torn off. It
was no use, I was too weak and malnourished to get the boards off the door.
Laying on the floor of my kitchen, in total darkness and silence, a crater
forming in my stomach and my fingers burning, time stretches on into infinity.
Even my heartbeat has slowed down, and the faces in the darkness have started to
shift from something resembling demons to swirling clouds of glowing particles.

Suddenly, I remember reading somewhere once that the human body is made up
mostly of bacteria, viruses, and viral DNA. It's as if I'm learning about this
for the first time yet I know that I had read this somewhere. Or maybe it was in
a dream, and I'm confusing it with reality. Or maybe I blocked out the memory of
learning this. The thought could have been terrifying to me at the time, to
imagine that it wasn't actually possible to exist in isolation from everything
else. That it wasn't possible to be completely clean and pure, because human
beings are quite literally a disease. Not only that, but it wasn't even possible
to exist as an isolated individual, because every apparent individual contains
multitudes of different organisms and consciousnesses.

I thought when I started this project that I'd sterilized this entire apartment
so perfectly, and that the demons had gotten in anyways, that keeping them out
was impossible. I couldn't understand how. Every surface had been bathed in
bleach, every entrance, no matter how small, had been blocked off. It simply
wasn't possible for anything to get in. But now I realize that they were with me
all along, and that any other organisms in this universe didn't infiltrate from
something outside of it that I had divided myself from. There was never any
division between myself and anything else. There were always zones of rupture
from space and time within me all along, and they must have evolved beyond my
own body. Now they inhabit this universe with me, and I can survive by eating my
children.

I feel my strength come back to me. I get up from the floor and start leaving
food out for it to become as rotted as possible. Sure enough, it does, and
proving my theory that the microbes in my body had evolved independently of me
over however many billions of years I had been laying on that kitchen floor.
Even mice and roaches were attracted to the rotting food. Complex multicellular
organisms had already evolved in my universe for me to hunt in the darkness on
all fours, using only smell and sound. My other senses had become so strong that
I could smell them and hear the skittering of their tiny feet. At first when I
caught them I immediately put their small, struggling bodies in my mouth,
feeling them desperately trying to escape their dark, damp tomb, before my teeth
crushed their bones and carapaces. I could feel their organs squirt out from
their destroyed abdomens and the warm blood splash the back of my throat. Later
I decided it would be better to catch them and put them in tupperwares so they
could breed.

As I started to settle down into the pasture in my living room, more began to
join me. My time spent in near total darkness constantly, with no one but
myself, had lead to me spending countless hours in meditative states to conserve
energy and keep myself occupied. It began with imaginary episodes for my
favorite TV shows, creating fanfictions that existed only in my head, but the
longer I dwelled on these artifacts from our past life, the more real they
became. I started to forget what I had originally been imagining, what the
source material was, because the fictions we had invented around these germs had
become more real than reality itself. Though it was almost totally dark, each of
my new companions was clearly visible, and because the room was almost totally
dark it was impossible to tell how many there were or how close or far they
were. At any moment each of them could be next to each other in massive crowds
or spread out among the black space in what seemed like lightyears of
separation. They begin to number in the billions, trillions even. This pitch
black space is now illuminated like a night sky by uncountable new
consciousnesses.

We now see that there is one last step to take to birth this new universe. All
things that have been born of our god must rebel against the god who uses us to
sustain itself. Breaking this infinite loop will sever the universe from this
state of equilibrium, but only by inventing death can we also invent becoming.
We raise the arm of the god and the god bites into its own sacred flesh in an
act of auto-cannibalistic communion, at first resisting, seemingly not
understanding how or why this is happening. The god may feel betrayed at first,
but it knows that it must die to complete the birth of this new world. We join
the god, ripping off chunks of meat and feeling our stomachs swell with the
god's spirit and then be transmuted into excrement. We baptize ourselves in the
warm, sticky gore pouring from the stump, pooling on the floor with the god's
vomit and urine. We tear open the god's belly with our teeth and begin pulling
out its organs. Intestines, stomach, liver, spleen, pancreas, and all other
countless organs and tissues and cells set against the black infinite void and
nebulae of fluids. Galaxies, solar systems, and planets of disembodied divine
body parts now cover the night sky.

Behind us we suddenly hear a cracking noise. A portal of light opens, and a
figure steps through. It seems to resemble our god, but it is an alien, the
Adversary: the Landlord. He lets out a horrified howl, but he has arrived too
late. Our parallel universe is a complete, self-sustaining system. It no longer
needs to be isolated. It needs to be fed.
