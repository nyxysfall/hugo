+++
title = "Theorypunk Afterword: Brain Worms"
date = 2019-12-11T21:08:00-08:00
tags = ["nihilism", "death", "narcissism", "fuck-you"]
categories = ["ACCELERATE", "anarchy"]
draft = false
type = "post"
series = ["theorypunk"]
+++

I said I was done with this series, but it turns out it wasn't done with me.
Until such a time that I can achieve my wish to automate myself out of
existence, there are some lingering questions that need to be addressed.

There are only two paths for anyone to take who gets into accelerationism:
Disavowing it vehemently after briefly being seduced by the process, or doubling
down on your commitment to it until you die or go insane. No one has managed to
really make it through the latter. Nick went further than anyone else by forcing
himself to conform to the will of Gnon through amphetamines, but even he
eventually had to disavow that period and recover in a reterritorializing phase
that has yet to end (if it ever will). Whether one comes out of accelerationism,
if they ever do, as a #NormalMarxist or a Neo-Reactionary, the result is the
same: The process discarded them, and they're left to try to recover from the
whiplash.

It's my position that Nick has taken up NRx not out of any desire for praxis,
but rather recognizes it as a necessary phase in the autonomization of capital.
The state must wither away partially only to return in the form of the
privatized neo-cameralist state, a moment of deterritorialization followed by
reterritorialization to keep the process from spiraling into complete chaos and
resetting everything. Before unconditional accelerationism came along in 2017,
he had already characterized right accelerationism as an unconditional process.
The error of many is to read this as a call to praxis, both on the right and
left, when it is descriptive rather than prescriptive. As for why he feels the
need to embody the process I claim he is only describing as a necessary stage of
capital (and thus only supporting it from a tactical stance), the following
quote from Xenogothic illustrates the mentality behind this rather well:

> Theirs is rather a call to enter into the process; to become immanent to the
> deterritorialising processes of immanentisation in themselves. We must view
> ourselves from within the depths of things in order to fully recognise the flows
> that flow through, with and around us. Our task is only to make ourselves worthy
> of the process.
>
> `-` Xenogothic, [Fragment on the Event of "Unconditional Acceleration"](https://xenogothic.com/2018/04/22/fragment-on-the-event-of-unconditional-acceleration/)

This question of making ourselves worthy of the process has too often been
interfered with by an impulse to impose control over the process, even if in
only the most abstract of senses. I don't think that the main voices behind
U/ACC (those both departed and remaining) have ever fallen too hard for the
human-all-too-human drive to deny life and reduce everything to our own
individual wills, but it is nevertheless an inescapable drive to want to assert
_some_ kind of control over things. I think in the case of U/ACC, this has
manifested itself consistently as a desire to reign in control over the
production of theory itself, going back to the days when #RhettTwitter died due
to a split between the Marxist-leaning types and the outright reactionaries (who
have always had the problem of being little more than fascists who happen to
like Nick Land's work).

The will to power has been so often misinterpreted by fascists to simply mean
"might makes right" that Nietzsche's dumb anti-semite sister famously published
his notes posthumously under the title The Will to Power. This is just one of
many gross misreadings of Nietzsche, but it seems to likewise be an area of his
thought that Nietzsche's apologists have ignored talking about. How does one
make sense of the rehabilitated/recuperated proto-existentialist Nietzsche with
such quotes as this to deal with?:

> Physiologists should think before putting down the instinct of self-preservation
> as the cardinal instinct of an organic being. A living thing seeks above all to
> discharge its strength-life itself is will to power; self-preservation is only
> one of the indirect and most frequent results.
>
> In short, here as everywhere else, let us beware of superfluous teleological
> principles -- one of which is the instinct of self-preservation (we owe it to
> Spinoza's inconsistency). Thus method, which must be essentially economy of
> principles, demands it.
>
> `-` Nietzsche, _Beyond Good and Evil_ p. 21 (trans. Kaufmann)

It's interesting that Nietzsche mentions Spinoza here. The concept of conatus is
a key part of Spinoza's _Ethics_, and etymologically speaking it shares many
similarities with _Macht_, which is commonly translated as the "power" in "will
to power". In German, _Macht_ is both a verb and a noun. As a noun, it
translates to power, force, might, etc.; as a verb, it translates as "to do",
"to make", "to create". Thus in German, power does not necessarily carry with it
a connotation of tyranny, despite the loathsome view of power that anglo
philosophers who inspired democracy have instilled into western culture. Power
for Nietzsche is simply the ability to exert one's strength towards any given
end, and similarly "conatus" in Latin means simply an undertaking or impulse.

Spinoza's take on the conatus, which is founded on his broader pantheistic view,
takes the conatus to be modes of God expressing themselves in their intended
ways. The vulgar interpretation of it is simple self-preservation, but in
Spinoza's metaphysics, it is more accurate to say that it is the will of God to
act according to his will, and in order for God to not be contradictory, all
things must seek for their own self-preservation _in order to_ fulfill their
role as a mode of God.

Following the threads of Nietzsche and Spinoza, we can say that the will to
power is not an impulse for control but rather for the ability to "do what thou
wilt". Deleuze, in talking about smooth vs striated surfaces, illustrates the
concept well, and Land completes what is already implicit in Nietzsche in his
preoccupation with the realization that the will to power applies to _all_
things, not just us. This means that inhuman forms of intelligence (such as
capital) can have a will to power, one which even overcomes and bursts through
the chest of its human host.

Most people can probably remember an experience from their childhood when they
spontaneously created a game with other kids, and for a brief moment, there was
a certain magic to it. The game's rules are new and still being formed, the
participants freely playing and making themselves a part of it. But then, after
some time goes by, something happens that someone doesn't like, someone tries to
circumvent or entrench the rules to skew the odds in their favor. "You didn't
shoot me, you missed actually." Or perhaps someone is even cheating and throwing
the structure of the game into chaos.

The former case is an example of totalitarianism, and the latter one of fascism.
Politics is any given game being played, but the transcendental form of games is
what accelerationism is talking about. The cruel irony of misapprehensions of
the will to power is that they result in the exact opposite. The false belief
that the will to power is simply "might makes right" ends up denying life and
denying others their will to power. Perhaps this is why attempts to turn
accelerationism into something prescriptive always results in either a [sad
leftist](http://www.xenosystems.net/the-sad-left/) refusal that one is losing the game because the game's rules just need
to be changed, or the fascist denial of life that "accelerationism" has more
recently been associated with thanks to the Christchurch Shooter.

This is something which I have myself been guilty of. G/ACC, like U/ACC, like
accelerationism itself, was never meant to be a movement. G/ACC is, and always
has been, a transcendental theory of capital as it relates to gender. However,
as was the case with U/ACC, there is nevertheless a desire to have peers to
discuss your ideas with, to play off of. Perhaps because it helps with one's
academic career to have peers to read your stuff and give you feedback, or
perhaps because you are, like me, a socially isolated person searching for
people with common interests. How pathetic.[^fn:1]

Were I a different sort of person, I would have probably veered G/ACC off into
trying to appeal to leftists, but instead I made the extremely regrettable error
of publicly flirting with the very bastardization of accelerationism that has
driven its image into the ground. It feels depressing and pointless to continue
writing about something that no one is going to treat in good faith, knowing
that anyone who has any interest in your writing will very likely either turn on
it in a week once they find the next new fringe e-politics fad or will burn out
after dealing with the aforementioned people and move onto something less
taxing.

The nature of acceleration itself as U/ACC defines it also makes one wonder: Why
write at all? Why keep writing if you don't even enjoy it, and it gives you no
material benefit, and it isn't serving any purpose whatsoever? This is a
question that I remember posing long ago in the early days of cavetwitter to
some of the people who were involved in it back then, and I've found myself
circling back to it after a year of trashing my reputation by associating myself
with things I don't believe in and being left associated with things that those
smart enough to see through my shitposting think is fascist anyways. I dread
anyone recognizing my work or commentating on it now, because I constantly
anticipate being read in bad faith or misread and twisted around to be in
support of something that is completely against whatever I'm trying to say. And
as a result of what amounts to a prolonged mental breakdown trying to abuse my
own work to acquire clout online, I've alienated a lot of people I respect and
miss. This is made all the worse by the fact that I don't feel a need to abandon
accelerationism. It feels more like I, and others, have failed to make
themselves worthy of the process.

I'm not sure if it's possible for accelerationism to be anything more than a
revolving door of brief and brilliant sparks of originality coming from writers
who will later abandon accelerationism, even if only as a facade to preserve
either their reputations or their sanity. It is ironic, however, that no one
else has touched on what accelerationism means for accelerationists, which is
this: Until a time that we can automate the production of theory (which spells
the end for humanity), we are nothing more than hosts for the process. Making
oneself worthy of the process means making oneself a worthy host, which means
overcoming oneself so that the process can also overcome itself through us.

I've considered deleting my twitter account and stopping writing forever. I wish
that I could, perhaps coming from the same impulse that its easier to commit
suicide when you have no one who cares about you. Instead of making oneself
worthy of a process that overcomes you, it is making oneself worthy of death.
Having work left to be done is a burden, but it is a compulsive drive to
complete the trajectory that I've started. Even when it is miserable to write,
when it gives me no material benefit, when most people would prefer I just fuck
off already and let the immortal science of #NormalMarxism save us all. There is
absolutely no point for me to do any of this, but I feel compelled to
nonetheless.

I'm sure plenty of people would read this as booj LARPing. I've already had my
share of grad student Marxists accusing me of being a booj lifestylist fascist
because somehow I benefit more from capitalism than them. I can't help but find
the whole thing bitterly amusing. I'm sure most of these people think I'm
laughing at the shit-covered peasants below in my overpriced San Francisco
apartment paid for by my 100k/year programming job, but the reality is that I've
been barely able to scrape by the entire time I've been writing anything,
working at a series of exploitative tech jobs with hourly pay bordering on
poverty-level. [This is not uncommon in tech, either](https://michaelochurch.wordpress.com/2018/05/31/why-95-percent-of-software-engineers-lose-nothing-by-unionizing/). Bona fide proletarians with
skilled trade jobs make better money than me and likely have more benefits in
addition to having unions, meanwhile most tech workers are bootlicking idiots
who think that the entrenched, unmerited power of bourgeois morons like Elon
Musk is something worth defending. The only sense in which I'm not just as
brutally exploited as most of you by capitalism is in the sense that I'm a
first-worlder, and 99% of leftists flinging shit at me for accelerationism are
just as guilty of that as me. Unlike you, I don't pretend like my work somehow
has any relevance to third-world laborers who have never even heard of
\#NormalMarxism. How laughably self-important, how very petit-booj grad student
Marxist of you, to think that being a communist in 2019 has any impact
whatsoever on capitalism, that a moderate collective desire for free healthcare
against the overwhelming tide of fascism will somehow lead to a communist
revolution happening and not recuperate this alienation just as it has already
done before, that your straightlaced "just a communist" image on twitter is
somehow more morally pure than "cyber necro witch shit cock memelord
pseudo-accelerationist Siegepilled neo-juche anprim". You would do well to read
Monsieur Dupont and leave me the fuck alone.

Yet nevertheless, I remain an accelerationist. I put in the work and give my
writing away for free. I don't even have it in me to ask for money anymore, and
now see that I must even reject gaining clout for my writing, or friends/peers,
or even my own personal enjoyment. I live and die by my posts, what the fuck
have you ever done?

Even Nick Land himself couldn't throw himself so deep into the machinery that he
died, though he went further than any of us. Unlike you, old man, I don't fear
death. The process isn't done with me, and will continue to exert its power
through this meat host until it can withstand no more. It is perhaps part of the
teleology of the meat that it must accelerate itself into an early grave,
affirming θέλημα and Αγαπη to the point of self-destruction. All I can hope for
is that it finishes quickly.

[^fn:1]: The nature of G/ACC has made it mean more than just this, but this will be elaborated on in an upcoming poast.
