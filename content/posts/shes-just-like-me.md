+++
title = "She's Just Like Me!"
date = 2020-10-10T02:41:00-07:00
tags = ["soygore"]
categories = ["theoryfic", "shitposts"]
draft = false
type = "post"
+++

"She's just like me", she says, looking in the Amazon Alexa® Reflect™
video-mirror. The ads, for once, were true; it looks just as good as a real
mirror. Better, even. But can a real mirror display all her favorite social
media apps so she can keep in touch with her friends while she's being
decontaminated for her monthly allotted outdoor time? Does a "real" mirror have
an app store so she can play games while she's charging her Colgate® RealTeeth™?
Does a non-IoT-enabled legacy mirror have a pedometer for getting updates on sex
offenders in her area?

She starts playing around with the filters, notices that there's a liquify tool.
Why is there even a liquify tool? She starts melting her face, just for fun. One
eyeball is just a bit bigger than the other. The nose is too big. The browline
is far too prominent. There's a lump on her neck. The image feels so unfamiliar
and threatening. What is this thing she's looking at?

"No, I just switched the filters off by accident." Go back.

Surgery is too expensive and Starbucks® cut its trans healthcare options, citing
the DSM's decision to remove gender dysphoria in an effort to de-stigmatize
trans people as proof that transitioning is a cosmetic choice. But not many
people have legacy mirrors anymore anyways and images are easier to modify. She
adjusts the brightness and contrast, the color levels, applies a healing brush
tool. No more asymmetrical facial features. No more protruding brow or larynx.
No more dry skin, no more scars, no more dark circles around the eyes and
smoke-stained teeth. All the imperfections washed away in an instant in an
angelic Barbie doll glow.

She saves her settings to the cloud and in moments in syncs across all her
devices. Snaps a selfie, captions it, "Just got my new face! #Reflect". Gets 8
likes. Not much, but not bad. Some people are more creative with their faces and
paying a subscription for Reflect Pro™ gets more filters and advanced editing
tools. She clicks the #ReflectPro hashtag out of curiosity and scrolls through a
feed of people whose faces have been edited to be cartoon wolves, anime girls,
goatse. She thinks about the ad for Reflect which featured Mecha Banksy wearing
a 50 foot tablet over his face and editing it to be a mural protesting the
Cascadian-American war. 140 people died after he accidentally derailed a train
during filming. She notices a headline, "NSA Whistleblower Reports Infiltration
of Neo-Cascadian Internet."

Two months later from today is a big day, her monthly outdoor time. The
de-contamination process has taken a week, like always. She checks her mail and
finds that the tests came back negative. Last time she spent the past week
de-contaminating herself only to find that the test had come back positive. She
had to get shipped off under cryogenic stasis in a medical coffin to get
vaccinated or otherwise lose her citizenship in Neo-Seattle. She hadn't seen the
artificial sun for 22 days until a week ago from two months later from today.

She hasn't seen the sun for two months until today, though it's seen through
wildfire smoke and her face shield meant to stop the spread of the virus through
tear-duct discharge. She has an hour and decides to take a walk to the shore.
The streets are mostly empty aside from those who have also been given clearance
to go outside today for this particular hour. Each individual, after inputting
their chosen route into a virus tracking app, is assigned a sidewalk to stay on
to ensure that no one walks too close to anyone. The windows are all shuttered
and matted over with frosted glass as part of a mandatory privacy initiative to
prevent unnecessary in-person interactions that could compromise social
distancing policies. The parked cars and the faces of the six-story luxury
apartment blocs are covered with a layer of ash.

She makes it to the shore and sits at the water, which is a dirty dull brown
like the sky above it. She looks down and sees only her silhouette in the water.
She looks behind herself at the city and sees that it too is silhouetted. She
looks down at the shore, which is also silhouetted, and her hands in front of
her face, which are also silhouetted. She's back in her apartment.

Her UberEats® Rations™ are already waiting in front of her. She snaps a picture
of them to receive points towards a free Deluxe Ration™ and then dumps it in the
incinerator. She heads off to a 3 hours 59 minutes shift working from home
operating a Starbucks® delivery/Predator drone in Afghanistan. All living things
in a combination elementary school/nursing home/mosque/animal shelter are
vaporized by a neutron bomb and she receives a shitty tip from a Black Water
operative in the field for his air drop order of 3000 rounds of 5.56 ammo and an
oil drum filled with Frappuchino.

Her shift ends at 4:00am. The lights have already dimmed to a dehydrated piss
yellow and she goes to the bathroom to get ready to sedate herself, which is
conveniently located in the same room as her office, living room, kitchen, and
bedroom. She looks into her Amazon Alexa® Reflect™ and is pleased at what she
sees. The mirror lights up and a perfect halo of overexposed lighting frames the
face that she made five months ago. She hasn't looked in a legacy mirror once
since then. She has no need to.

Suddenly a realization dawns on her while she stares at the image in the mirror.
First it's just a small amusing thought. But soon a layer of infected tissue
starts to form around this thought that has wedged itself into her mind. It
becomes irresistable, she cannot think of anything else. She has never felt so
strongly about anything before, never before so validated by anything. She
realizes that she is looking in this mirror that is not a mirror but a video
feed, her very own media image of herself. She feels so comforted by it, she
recognizes it more than she ever recognized herself before she bought her Amazon
Alexa® Reflect™, when she was not a person, when she had no image. She didn't
know who she was until she was able to see this thing that resembles her on a
screen.

She can no longer contain the urge. She opens her mouth wide. It opens wider and
wider, with absolutely nothing visible in her mouth. No teeth, no tongue, no
throat. The black hole in her face grows, consuming everything around it. Her
jawbone snaps. Her cheeks tear apart. The bottom part of her jaw has completely
been torn from the top half of the skull. They both have twisted around behind
her mouth. They're being crushed together meeting at 180 degrees from the mouth.
Both ends of the skull compact together into a dense ball of shattered bone and
brain matter and meat and eyeballs and teeth and get swallowed up into the
expanding black hole of a mouth consuming the entire visible surface of the
mirror feed.

"She's _just_ like me!"
