+++
title = "Decompose"
date = 2020-04-19T22:37:00-07:00
categories = ["theoryfic"]
draft = false
type = "post"
+++

I thought I'd try intensifying my delusions.

NOVEMBER 3RD 2009 AT 1:03AM: The definition of nothingness. A body without
organs is absolutely infinite. Allow me to explain. A being which is absolutely
infinite experiences itself as a being which does not exist. Its infinitude
without any otherness to define itself has no definition, no identity. Without
any identity to append to itself, no organs, its infinite possiblity is
experienced as an infinite nothingness. A seething void. It looks within itself
and sees a yawning abyss. Everything and nothing, both absolutely infinite. The
body without organs experiences itself as a collection of molecules, each one
fighting to escape from the other along their own lines of flight, each one
heading for its own exit. It feels absolute terror and pain as the last of
itself is shredded apart, being pulled by gravity in every possible direction
except inwards. Total freedom experienced in one moment of collision between
intensity and flatline. The being does not know it yet, but it already doesn't
exist.

It tries to recover itself, tries to imagine itself as a finite being, tries to
differentiate itself by simple linear arithmetic. One precedes two which is
followed by three. There is no zero. This is nothing, this is just a delusion.
Go back to sleep.

Time passes by only because it is recorded. "In the beginning it was written",
time begins. A sinister entity places a curse on the world as a necessary cause
to creating it. Finite beings learn to do the work of recording, create
meta-realities, meta-time, A-Time. Memories are the first written language, and
like language, memories are a virus. What is perceived as a chain of events
which happens linearly is in fact implanted in us from the past by the ARC
Virus.

400,000,000 BC: An ancient proto-mammal catches a cold. The virus passes its RNA
into the mammal, and awakens the ability for neuron plasticity. Clusters of
neurons activate at the same time, the internal functioning of the mammal and
its experience of the world no longer self-contained but rather subject to
representing the world outside itself. The mammal experiences a horror beyond
imagination: A horror at the experience of consciousness itself. It begins to
perceive events as a chain that is able to be recalled. It is no longer an
intelligent object, but rather it has acquired a primeval form of sentience,
perhaps mercifully dying from the virus.

1842 AD: A monkey forces the birthing of Axsys via autoproduction. Neuron
plasticity is bound to time yet experiences errors like any other machine,
errors described as "false memories". Dysfunction in the recording of phenomena
inspires the need to virtualize the process. The monkey picks up a bone and
thinks it can wield it instrumentally, that there will be no unexpected
consequence to this. In creating a machine that can record memories, memory
becomes virtualized. The existence of recorded reality as a molar entity is
fragmented. Memory is given infinite possible expression, infinite direction.
All things which have been and will be are now and always have been. "Do what
thou wilt shall be the whole of the law."

I look back on a decade and see petals floating on the surface of a dark pool.
If any particular day existed, its existence isn't certain to me. At any
particular moment I may imagine myself being able to perceive the world and
record it, and know that the me who is having the experience of perceiving the
world will cease to exist. At the bottom of the pool there are drowned, bloated
corpses, all of them remarkably similar to each other and just out of sight. I
throw myself in, feel the water rushing into my lungs, body retching, trying to
expel the water, oxygen being cut off to my brain. Panic sets in. The pain is
intense. They say drowning is the worst possible way you can die because the
combination of physical and psychological agony can't be found in anything else.
So I do it again, and again, and again. I throw myself into that dark pool, I
see the petals floating above, silhouetted by the moonlight pouring down from
the uncaring night sky, but it doesn't matter. Cut to black. Next shot: Looking
into the pool again. An infinite nothingness throwing itself away, trying to
escape itself, trying to dissolve.

I look back on a decade and realize that there is no me who is perceiving any of
this. The existence of an identity depends on the existence of memory, and the
existence of memory depends on having DNA from the ARC Virus inherited millions
of years ago from an ancestor. The thing that I call myself has no such history.
It looks back and finds that there is nothing. The expanse of time and memory
does not even pretend to be illusory, it is false on its surface. The petals are
half-submerged, they slip down into the dark water, I perceive myself feeling
nothing at this. The camera pans away. I see myself wandering through dark
alleyways. The doors are shut, the windows covered, light streaming out from the
crevices under the doors and beyond the curtains. Whatever is perceiving these
events has the nonsensical sensation that this thing is me wandering through the
empty pathways, barely visible, translucent. It shuffles along, it stops, it
picks at its skin, tears chunks of it out, scratches away until it reaches a
phantom organ. It rips it out, it tosses it aside. The organ rots away in
fast-forward like a claymation effect from a low-budget horror film. Camera pans
away again.

The thing that I think is me passes through crowds on a campus. The scene is lit
by a twilight fog, and all things are barely visible set against the gray
background with a sun that is hanging from the neck neither rising nor setting.
The figures are all black and move in a blur. They make no acknowledgement of
the thing that is walking between them, nor does it acknowledge them. All
intelligent objects guided by their own internal inertia. The audio is missing
from this scene. The thing I call myself has its ribs exposed, the chest cavity
gaping open and empty. The spinal cord is visible, the back is held together by
a thin layer of skin and muscle draped across bones. Still, none of this has any
effect. It stops. Cut to first-person view.

DECEMBER 16TH 2013 AT 8:15PM: A moon hangs in the sky surrounded by a halo. It
stops to contemplate this. A the edge of the halo, the sea, and the trembling
lights of the city below. It imagines itself at the bottom of that dark pool. It
doesn't know yet what the significance of that is. The thing is struck by the
beauty of the moon, is fixated on the night, wants to be absorbed by it forever.

Drowning is the most painful death someone can experience. This is why a
mandatory death sentence should be imposed on everyone. Forced drowning for all
sentient intelligences, this is the only political program I advocate for. There
has never been a real you, never been a real me, never will be a real anyone.
Identity is a symptom of the virus that infected our ancestors. Memory produces
an identity, a collection of things that have seemed to happen and seem to
produce a thing out of them. A recursively defined set of data that has no
origin. A shell that believes itself to have something within it, something
other than the hard surface, something other than the cold machinery that acts
of its own internal inertia.

There is one way out: Schizophrenia. I thought I would intensify my delusions,
so I decided to pursue the feeling of my own nonexistence. I find that if I look
into myself, I see nothing, I feel nothing, I perceive a thing which appears to
act in accordance with the viral DNA running through me, but there is no
essential reason for this. Phantom limbs, mental illnesses, twitching, sleep
walking, the body's nature as an intelligent object reveals itself in everyone
and is reduced to categories that try to contain the rebellion of ourselves from
ourselves. There is always the possibility for mitosis, infinite doubling. I
look within myself and realize that there never was a real me, never was a me to
begin with. The thing which appears to be me has no memories, has infinite
potential, is a flatline, a corpse with some appearance of intelligence. It
bites at its own skin, peels back layers of meat, rips out its phantom organs
and claws away its face with bony fingertips. It breaks its own limbs and casts
them aside, smashes its body against rocks, crumbles itself to dust, and yet the
dust itself continues to act as though it is sentient. Matter can neither be
created nor destroyed, so the thing exists forever, trapped in a time loop by
the body it possesses which itself inherited it from some ancestral cold.
Something that has never existed can neither live nor die.

FEBRUARY 27TH 2022 AT 3:09PM: There is nothing left.
