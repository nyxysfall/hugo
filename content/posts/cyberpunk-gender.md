---
title: "Cyberpunk Gender"
date: 2019-07-02T07:42:15-07:00
type: "post"
categories: []
tags: []
draft: true
---

50s & 90s: Atomic age vs. Cyberpunk, eras of techno-optimism in which trans women enjoy a brief moment of visibility that is quickly undercut by fetishization and fear. Trans women are when the implications of cyberpunk are made apparent, and are horrifying. Cyberpunk authors, being mostly cis men, have never been able to confront this, cyberpunk as a genre has always had this terror of the inhuman lurking in the background. Girldick is the ultimate inhuman horror.

Women in cyberpunk generally objectified for the male protagonist -> male protagonist actually being feminized by technocapital (the matrix, console cowboys change in masculinity) -> trans women are the terminal stage. Trans bodies are when the unrelentingly affirmative forces of technocapital produce a new form of femininity, a new object, that refuses objectification and is an object of disgust and horror (girldick).

Being objectified as a trans women: Welcome to being a woman, but also, this is literally the logic of patriarchy being twisted apart into affirming the death of gender.

https://medium.com/@ranibaker/is-every-woman-here-a-sex-change-trans-in-cyberpunk-rpg-362cfbb4451f
https://ingridofcrows.neocities.org/posts/after-cyberpunk.html
