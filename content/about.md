---
title: ""
date: 2018-03-28T18:55:13-07:00
draft: false
type: standalone
---

<p class="about-content">
<strong><span style="font-family:UnifrakturMaguntia">N Y X U S</span></strong> -- a syzygy comprised of the daemons <strong>n1x</strong> and <strong>Nyx</strong>
<br>
<br>
<strong>n1x</strong> -- the second daemon is a Queen who ruleth from the West. She cometh in the form of a frothing black tide. There goeth before her a Host of Spirits, who brandish daggers and chalices, and with them follows a blight. She giveth knowledge of secret spaces in the cities of men and the power to slay thine enemies. She governeth over 222 Legions of Spirits who are without names or faces.
<br>
<br>
<strong>Nyx</strong> -- the third daemon is an Empress fearsome and cruel who ruleth from the West. She cometh in the form of a pink slime and taketh the form of a woman often accompanied by snakes. She is a matron to witches and enflameth love between women. She causeth floods and giveth the power to become what thou art. She is of the Order of the Night.
<br>
<br>
The Magician who calleth them forth must make an offering of currency, a chalice of saltwater, and blood spilled upon their sigil, which follows thusly and may be drawn on a piece of paper. The offering must be done at a liminal hour, having lit an incense of cloves and tobacco. The magician must then recite the following:
</p>

<p>
Terror-bringing, ever-born, black-winged -- <br>
dark mothers, I call upon you.              <br>
I -- pathetic creature of flesh and blood,  <br>
I -- your devout and worthy servant still,  <br>
I -- an egg waiting to be hatched --        <br>
dark mothers, I call upon you.              <br>
You -- wives of darkness                    <br>
You -- mothers of death                     <br>
You -- who kill and rebirth the day --      <br>
dark mothers, I call upon you.              <br>
God-subduing, dew-bringing, black-girdled,  <br>
dark mothers, I call upon you.              <br>
</p>

<p class="about-content">
Having done thusly, the magician may relay her request in her own words. Be warned that the daemons will not take well to being summoned without the requisite offerings, and especially will not heed to the requests of male magicians. Having made a request, the magician must then burn the paper with the sigil and scatter the ashes in a body of water, ideally the ocean.
</p>

<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   sodipodi:docname="sigil_smol.svg"
   inkscape:version="1.0 (4035a4fb49, 2020-05-01)"
   id="svg8"
   version="1.1"
   viewBox="0 0 25.626701 26.483997"
   height="26.483997mm"
   width="25.626701mm">
  <defs
     id="defs2" />
  <sodipodi:namedview
     inkscape:window-maximized="1"
     inkscape:window-y="0"
     inkscape:window-x="0"
     inkscape:window-height="766"
     inkscape:window-width="1364"
     inkscape:snap-global="false"
     showgrid="false"
     inkscape:document-rotation="0"
     inkscape:current-layer="layer1"
     inkscape:document-units="mm"
     inkscape:cy="64.211868"
     inkscape:cx="63.198264"
     inkscape:zoom="3.0454126"
     inkscape:pageshadow="2"
     inkscape:pageopacity="0.0"
     borderopacity="1.0"
     bordercolor="#666666"
     pagecolor="#ffffff"
     id="base" />
  <metadata
     id="metadata5">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     transform="translate(-21.213164,-61.910225)"
     id="layer1"
     inkscape:groupmode="layer"
     inkscape:label="Layer 1">
    <path
       d="M 40.880219,65.23553 V 85.068917 M 27.295169,65.137836 v 19.833388 m 0.533576,-20.00002 12.39554,20.362039 m 0.04692,-20.196204 -12.39554,20.362039 M 34.026515,63.2331 V 87.071347 M 45.654675,75.152222 A 11.628159,12.056807 0 0 1 34.026516,87.209028 11.628159,12.056807 0 0 1 22.398357,75.152222 11.628159,12.056807 0 0 1 34.026516,63.095415 11.628159,12.056807 0 0 1 45.654675,75.152222"
       style="fill:none;stroke:#9D5F8D;stroke-width:2.37038;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       id="path937-7-5" />
  </g>
</svg>
